const amazonUrl = "https://s3-eu-west-1.amazonaws.com/usr21-demobucket/";
var proxy = require('express-http-proxy');


module.exports = {
    hello: function() {
       return "Amazon url: " + amazonUrl;
    },

    amazonProxy: proxy(amazonUrl, {
        proxyReqPathResolver: function(request) {
          const id = request.query.id;
          console.log("documentid: " + id);          
          return amazonUrl + id;
        }
      })
}