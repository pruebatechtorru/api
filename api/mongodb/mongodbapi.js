//js as a lib to connect to our mongodb

//mongodb
const mongodb = require('mongodb');
const mongoClient = mongodb.MongoClient;
const url = process.env.mongodburl;


module.exports = {
    hello: function() {
       return "Mongo db is listening, url: " + url;
    },

    obtainTransaction: function(req,res){
        mongoClient.connect(url, function(err, db){
            if(err){
                console.log(err);
            }else{
                console.log("connected");
                const collection = db.collection("movimientos"); 
                var query = {};
                if(req.query.id != undefined){
                   query = {id: req.query.id};
                }               
                console.log(query);
                collection.find(query).toArray(function(err, docs){
                    console.log(docs);
                    res.send(docs);
                });
                db.close();
            }
        });
    },

    deleteTransaction: function(req,res){
        mongoClient.connect(url, function(err, db){
            if(err){
                console.log(err);
            }else{
                console.log("connected");
                const collection = db.collection("movimientos"); 
                var query = {};
                //delete collection by id
                if(req.query._id != undefined){
                   query = {_id: new mongodb.ObjectID(req.query._id)};
                }               
                console.log(query);
                collection.remove(query,{justOne: true}, function(err, results) {
                    if (err){
                      console.log("remove collection failed");
                    }else{
                       console.log("remove collection done");                       
                    }                                
                    res.send(results);
                });
                db.close();
            }
        });
    },

    insertTransaction: function(req,res){
        mongoClient.connect(url, function(err, db){
            if(err){
                console.log(err);
            }else{
                console.log("connected");
                const collection = db.collection("movimientos");             
                console.log(req.body);
                collection.insertOne(req.body, function(err, results) {
                    if (err){
                      console.log("insert collection failed");
                      console.log(err);
                    }else{
                       console.log("insert collection done");   
                       console.log(results);                      
                    }                                
                    res.send(results);
                });
                db.close();
            }
        });
    },


}

/*
var mongodb = require("mongodb");

var client = mongodb.MongoClient;
var url = "mongodb://host:port/torrumongodb";

client.connect(url, function (err, db) {

    var collection = db.collection("movimientos");

    var query = {};

    var cursor = collection.find(query);

    cursor.forEach(
        function(doc) {
            console.log(doc["_id"]);
        },
        function(err) {
            db.close();
        }
    );

    // Created with Studio 3T, the IDE for MongoDB - https://studio3t.com/

});
*/