const requestjson = require('request-json');
const urlMovimientosMlab = "https://api.mlab.com/api/1/databases/torrumongodb/collections/movimientos";
const apikey = "apiKey="+ process.env.mlabapikey;


module.exports = {
    hello: function() {
       return "MaLab api created, url: " + urlMovimientosMlab + ", apikey: " + apikey;
    },

    get: function(request, response){
      var client = requestjson.createClient(urlMovimientosMlab);
      const data = '?&q={"id":"'+request.query.id+'"}&'+apikey;
      console.log("params get mlabapi:" +  data);
      client.get(data, function(error, resM, body){
        if(error){
          console.log(body);
          response.send(body);
        }else{
          console.log("request response " + body);
          response.send(body);
        }
      });
    },

    delete: function(request, response){
      var client = requestjson.createClient(urlMovimientosMlab+'/'+request.query.id);
      const data = '?'+apikey;
      console.log("params delete mlabapi:" +  data);
      client.del(data, function(error, resM, body){
        if(error){
          console.log(body);
          response.send(body);
        }else{
          console.log("request response " + body);
          response.send(body);
        }
      });
    },

    put: function(request, response){
      console.log("put id");
      console.log(request.query.id);
      var client = requestjson.createClient(urlMovimientosMlab+'/'+request.query.id);
      const data = '?'+apikey;
      console.log("put body");
      console.log(request.body);
      client.put(data, request.body, function(error, resM, body){
        if(error){
          console.log("put response error");
          console.log(body);
          response.send(body);
        }else{
          console.log("put response ");
          console.log(body);
          response.send(body);
        }
      });
    },

    post: function(request, response){
      var client = requestjson.createClient(urlMovimientosMlab);
      const data = '?'+apikey;
      client.post(data, request.body, function(error, resM, body){
        if(error){
          console.log(body);
        }else{
          response.send(body);
        }
      });
    }
}
