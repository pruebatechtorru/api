const pg = require('pg');
const pgString = process.env.postgredbrurl;

const pgClient = new pg.Client(pgString);
pgClient.connect();

module.exports = {
    hello: function() {
       return "Postgre db is listening, url: " + pgString;
    },

    obtainUser: function(request,response){
       //req.body = {email:xxxxx.password:yyyy}
        const query = pgClient.query('select * from users where email=$1 and password=$2;',
        [request.body.email, request.body.password],
            (error, result)=>{
                if (error){
                    console.log(error);
                }else{
                    console.log(result.rows[0]);
                    response.send(result.rows[0]);
                }
            }
        );
    },

    insertUser: function(request,response){
        //insert into users values ('torrubiano69@gmail.com','1234','Miguel Angel','Lopez Martinez','user');
         const query = pgClient.query("insert into users values ($1,$2,$3,$4,'user');",
         [request.body.email, request.body.password,request.body.name,request.body.surname],
             (error, result)=>{
                 if (error){
                     console.log(error);
                     response.send('{"message":"User not created","status":"0"}');
                 }else{
                     console.log("Registros insertados: " + result.rowCount);
                     response.send('{"message":"User created","status":"1"}');
                 }
             }
         );
     }

}