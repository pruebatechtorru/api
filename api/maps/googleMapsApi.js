const googleMapApiUrl = "https://maps.googleapis.com/maps/api/staticmap";
const apikey = process.env.mapsapikey;
var proxy = require('express-http-proxy');


module.exports = {
    hello: function() {
       return "Maps api created, url: " + googleMapApiUrl + ", apikey: " + apikey;
    },

    googleApiProxy: proxy(googleMapApiUrl, {
        proxyReqPathResolver: function(request) {
          console.log("resolver de red: " + request);
          const latitude = request.query.latitude;
          const longitude = request.query.longitude;
          const data = '?center='+ latitude +','+longitude +
               '&zoom=13&size=500x250&maptype=roadmap' +
               '&markers=color:red%7Clabel:X%7C'+ latitude +','+longitude +
               '&key='+apikey;
          return googleMapApiUrl + data;
        }
      })
}