var express = require('express');
var app = express();
var appProxy = express();
var port = process.env.PORT || 3000;
var port2 = process.env.PORT2 || 3001;
var path = require('path');
var bodyParser = require('body-parser');
var cors = require('cors')

app.listen(port);           
app.use(bodyParser.json());         // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));
app.use(cors());

appProxy.listen(port2);   //plain body with no json

const mongodbApi = require('./api/mongodb/mongodbapi.js');
console.log(mongodbApi.hello());

const postgredbApi = require('./api/postgredb/postgredbapi.js');
console.log(postgredbApi.hello());

const mLabApi = require('./api/mongodb/mLabapi');
console.log(mLabApi.hello());

const mapsApi = require('./api/maps/googleMapsApi');
console.log(mapsApi.hello());

const amazonApi = require('./api/amazon/amazonApi');
console.log(amazonApi.hello());


console.log('Server json REST API started on: ' + port);
console.log('Server proxy pass started on: ' + port2);

app.get('/', function(request, response){
  response.sendFile(path.join(__dirname, './index.html'));
});


//maps api
appProxy.use('/maps', mapsApi.googleApiProxy);

//maps api
appProxy.use('/documents', amazonApi.amazonProxy);

//mLab api
app.get('/movimientosPending', function(request, response){
  mLabApi.get(request, response);
});

app.post('/movimientosPending', function(request, response){
  mLabApi.post(request, response);
});

app.delete('/movimientosPending', function(request, response){
  mLabApi.delete(request, response);
});

app.put('/movimientosPending', function(request, response){
  mLabApi.put(request, response);
});


//mongodb api
app.get('/movimientos', function(request,response){
  mongodbApi.obtainTransaction(request,response);
});

app.delete('/movimientos', function(request,response){
  mongodbApi.deleteTransaction(request,response);
});

app.post('/movimientos', function(request,response){
  mongodbApi.insertTransaction(request,response);
});




//postgredb api
app.post('/login', function(request, response){
  postgredbApi.obtainUser(request,response);
});

app.post('/register', function(request, response){
  postgredbApi.insertUser(request,response);
});


