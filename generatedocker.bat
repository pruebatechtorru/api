rem build polymer
rem polymer build

rem kill old docker
docker kill server

rem remove old docker
docker rm server

rem create a new build
docker build -t torru/server .

rem start new container
rem --net techui 
docker run --env-file ./env.file -p 3000:3000 -p 3001:3001 --name server -d torru/server
